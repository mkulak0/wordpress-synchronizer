<?php 

namespace WordpressSynchronizer;

//require_once "config.php";

class AdminPage {

    public function __construct(){

        \add_menu_page( Config::$menu_display_name, 
                        Config::$menu_display_name, 
                        'manage_options', 
                        Config::$prefix,
                        //__NAMESPACE__.'\\AdminPage::show()',
                        array('WordpressSynchronizer\\AdminPage::class', 'show'),
                        '',
                        50
                    );
 
    }

    public function show(){
        echo "<h1>Hello World!</h1>";
    }

    public function get(){
        return $this;
    }
}