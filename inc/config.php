<?php

namespace WordpressSynchronizer;

class Config {

    public static $prefix = "wordpress_synchronizer";

    public static $default_category_slug = "do_synchronizacji";

    public static $default_category_name = "Do synchronizacji";

    public static function activate(){
        if(\category_exists(Config::$default_category_slug)){
            \error_log("Kategoria istnieje");
        } else {
            \error_log("Kategoria nie istnieje");
            \wp_insert_category( [
                'cat_name' => Config::$default_category_name , 
                'category_nicename' => Config::$default_category_slug
            ]);
        }
    }


    public static $menu_display_name = "WP-Synchronizer";


}