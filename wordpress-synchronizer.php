<?php

/*
    Plugin Name: Wordpress Synchronizer
    Description: Opis wtyczki
    Author: Maciej Kułak
    Version: 0.0.0
*/

namespace WordpressSynchronizer;

/* INCLUDES */

require_once __DIR__."/inc/plugin.php";



$WordpressSynchronizerPlugin = new Plugin();


\add_action( 'plugins_loaded', [ $WordpressSynchronizerPlugin, 'load' ]);

/* INITIALIZING */

\register_activation_hook( __FILE__, __NAMESPACE__.'\\activate');